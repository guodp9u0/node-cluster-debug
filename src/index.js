const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;

if (cluster.isMaster) {
    console.log('Master started')
    cluster.setupMaster({
        execArgv: process.execArgv.filter(function(s) { return s !== '--debug' })
    });
    for (var i = 0; i < numCPUs; i++) {
        cluster.settings.execArgv.push('--inspect=' + (5859));
        cluster.fork();
        cluster.settings.execArgv.pop();
    }
    cluster.on('listening', function(worker, address) {
        // console.log('listening: worker ' + worker.process.pid + ', Address: ' + address.address + ":" + address.port)
        // console.log(worker.process._debugPort);
    });
    cluster.on('exit', (worker, code, signal) => {
        // console.log(`worker ${worker.process.pid} died`);
    });
} else {
    let httpPort = cluster.worker.process.pid;
    try {
        http.createServer((req, res) => {
            res.writeHead(200);
            res.end(`hello world from ${httpPort}`);
        }).listen(httpPort);
        console.log(`${cluster.worker.process.pid} http server started on http://127.0.0.1:${httpPort}`);
    } catch (error) {
        console.error(error)
    }
}